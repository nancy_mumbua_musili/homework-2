from nose.tools import assert_equal
import aims                                   #this is a framework tool for testing 

def test_ints():
    numbers = [1, 2, 3, 4, 5,6]
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert obs == exp
    assert_equal(obs, exp)


def test_floats():
    numbers = [11, 20, 3, 4, 5,6]
    obs = aims.std(numbers)
    exp = 5.8713049846028458
    assert obs == exp
    assert_equal(obs, exp)

def test_floats1():
    numbers = [20, 24, 34, 41, 55,60]
    obs = aims.std(numbers)
    exp = 14.787382008545888
    assert obs == exp
    assert_equal(obs, exp)

def test_floats2():
    numbers = [0.1, 0.8, 0.5, 41, 50.1,60]
    obs = aims.std(numbers)
    exp = 25.546912185667804
    assert obs == exp
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00215']
    obs = aims.avg_range(files)
    exp = 5
    assert obs == exp
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00317']
    obs = aims.avg_range(files)
    exp = 6
    assert obs == exp
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00380']
    obs = aims.avg_range(files)
    exp = 9
    assert obs == exp
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00466']
    obs = aims.avg_range(files)
    exp = 5
    assert obs == exp
    assert_equal(obs, exp)
